// C++ program to find out execution time of
// of functions
#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>
#include <climits>
using namespace std;
using namespace std::chrono;

uint max_subsequence_sum_1(vector<int>&, int);
uint max_subsequence_sum_2(vector<int>& a, int n);
uint max_subsequence_sum_3(vector<int>& a, int n);
int kadane(vector<int> &a, int size);

// For demonstration purpose, we will fill up
// a vector with random integers and then sort
// them using sort function. We fill record
// and print the time required by sort function
int main() {
  vector<int> values(4000);

  // Generate Random values
  auto f = []() -> int { return rand() % 10000; };

  // Fill up the vector
  generate(values.begin(), values.end(), f);

  // Get starting timepoint
  auto start = high_resolution_clock::now();
  // Call the function, here sort()
  int val1 = max_subsequence_sum_1(values, values.size());

  // Get ending timepoint
  auto stop = high_resolution_clock::now();

  // Get duration. Subtract timepoints to
  // get duration. To cast it to proper unit
  // use duration cast method
  auto duration1 = duration_cast<microseconds>(stop - start);

  // Get starting timepoint
  auto start2 = high_resolution_clock::now();
  // Call the function, here sort()
  max_subsequence_sum_2(values, values.size());

  // Get ending timepoint
  auto stop2 = high_resolution_clock::now();

  auto duration2 = duration_cast<microseconds>(stop2 - start2);

  // Get starting timepoint
  auto start3 = high_resolution_clock::now();
  // Call the function, here sort()
  int val3 = kadane(values, values.size());


  // Get ending timepoint
  auto stop3 = high_resolution_clock::now();

  auto duration3 = duration_cast<microseconds>(stop3 - start3);


  cout 
      //  << "1: " << duration1.count() << " microseconds" << endl
      //  << "2: " << duration2.count() << "ms" << endl
       << "1: " << val1<< ", "<< duration1.count() << "ms" << endl
       << "3: " << val3<< ", "<< duration3.count() << "ms" << endl;


  return 0;
}

// #include<iostream>
// using namespace std;

// const int MAX = 1000000;

// int main(){
//   int a[] = { -2, 11, -4, 13, -5, -2}, n = 6;

//   cout << max_subsequence_sum(a, n);
//   return 0;
// }

uint max_subsequence_sum_1(vector<int>& a, int n) {
  int max_sum = 0, best_i, best_j;
  best_i = best_j = -1;
  for (int i = 0; i < n; i++)
    for (int j = i; j < n; j++) {
      int this_sum = 0;
      for (int k = i; k <= j; k++)
        this_sum += a[k];
      if (this_sum > max_sum) {
        /* update max_sum, best_i, best_j */
        max_sum = this_sum;
        best_i = i;
        best_j = j;
      }
    }
  return max_sum;
}

uint max_subsequence_sum_2(vector<int>& a, int n) {
  int max_sum = 0;
  int best_i, best_j;
  best_i = best_j = -1;
  for (int i = 0; i < n; i++) {
    int this_sum = 0;
    for (int j = i; j <= n; j++) {
      this_sum += a[j];
      if (this_sum > max_sum) {
        /* update max_sum, best_i, best_j */
        max_sum = this_sum;
        best_i = i;
        best_j = j;
      }
    }
  }
  return max_sum;
}

int kadane(vector<int> &a, int size)
{
    int max_so_far = INT_MIN, max_ending_here = 0;
 
    for (int i = 0; i < size; i++) {
        max_ending_here = max_ending_here + a[i];
        if (max_so_far < max_ending_here)
            max_so_far = max_ending_here;
 
        if (max_ending_here < 0)
            max_ending_here = 0;
    }
    return max_so_far;
}

uint max_subsequence_sum_3(vector<int>& a, int n) {
  int i, this_sum, max_sum, best_i, best_j;
  i = this_sum = max_sum = 0;
  best_i = best_j = -1;
  for (int j = 0; j < n; j++) {
    this_sum += a[j];
    if (this_sum > max_sum) {
      /* update max_sum, best_i, best_j */
      max_sum = this_sum;
      best_i = i;
      best_j = j;
    } else if (this_sum < 0) {
      i = j + 1;
      this_sum = 0;
    }
  }
  return max_sum;
}
